package sda.wzorce.liskov;

public class Main {

    public static void main(String[] args) {
        Rectangle r = new Square();
        r.getArea();
        r.setColor(250);
        setRectangleAndPrintArea(r);
    }

    private static void setRectangleAndPrintArea(Rectangle rectangle) {

        if(rectangle instanceof  Square) {
            Square s = (Square)rectangle;
            s.getSqareArea();
        } else {
            rectangle.setWidth(5);
            rectangle.setHeight(10);

            int area = rectangle.getArea();
            System.out.println(area);
        }

    }
}
