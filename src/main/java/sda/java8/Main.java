package sda.java8;

import jdk.internal.misc.JavaObjectInputStreamAccess;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class Main {

    public static void main(String[] args) {
        metoda("text");
        List<String> lista = new ArrayList<>();
/*        Consumer<String> stringConsumer = s -> {
            System.out.println(s);
            System.out.println(s + s + s + s);
        };*/
        //Consumer<String> myConsumer = new MyConsumer();

        lista.forEach(a -> System.out.println(""));
        lista.forEach(s -> {
            if(s.startsWith("Jarosław")) {
                throw new RuntimeException();
            } else {
                System.out.println("Wszystko spoko");
            }
        });

        mojaMetodaPrzyjmujacaLamde((s) -> s.endsWith("aaa"));

        ThreeArgumentsConsumer javaObjectInputStreamAccess = (a, b, c) -> {};
    }

    public static void metoda(String text) {
        System.out.println(text);
    }

    public static void metoda(double d, int i) {
        System.out.println(d);
        System.out.println(i);
    }

    public static void metoda(MojaZajebiszaczaMetoda zmetoda){

        System.out.println("Cos sobie robię");
        zmetoda.zajebiaszczaMetoda();
        System.out.println("Cos sobie robie dalej");

    }

    public static void mojaMetodaPrzyjmujacaLamde(Predicate<String> p) {
        boolean sfdgsfd = p.test("sfdgsfd");
        if(sfdgsfd) {
            System.out.println("asdfasdf");
        }
    }


}
