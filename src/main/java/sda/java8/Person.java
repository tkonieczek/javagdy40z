package sda.java8;

import java.util.Optional;

public class Person {
    private String name;
    private String surname;
    private int age;

    public Person() {
    }

    public Person(String name, String surname, int age) {
        this.name = name;
        this.surname = surname;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public static void main(String[] args) {

        Person janusz = new Person("Janusz", "Grażyński", 40);
        Optional.of(janusz).filter(person -> person.getAge() > 18).ifPresent(per ->{
            System.out.println(per.getName());
            System.out.println(per.getSurname());
        });
    }

    public static void metoda(String p, int per) {

    }


}