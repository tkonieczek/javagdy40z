package sda.java8;

@FunctionalInterface
public interface ThreeArgumentsConsumer<T, U, Y> {

    void accept(T t, U u, Y y);

}


@FunctionalInterface
interface ThreeArgumentsConsumer2 {

    void accept(String t, String u, int y);

}